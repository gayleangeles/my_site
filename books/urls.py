from django.conf.urls import patterns, url

from .views import search, detail, index


urlpatterns = patterns('',
  	url(r'^search/$', search),
  	url(r'^(\d+)/$', detail, name="detail"),
  	url(r'^$', index, name="index")
)