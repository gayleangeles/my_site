from django.contrib import admin

# Register your models here.

from .models import Author, Book, Publisher

class AuthorAdmin(admin.ModelAdmin):
    fields = ('email', 'first_name', 'last_name')
    search_fields = ['first_name', 'last_name']

class BookAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'authors', 'publisher']}),
        ('Date information', {
            'fields': ['publication_date'],
        }),
    ]
    list_display = ['title','was_published_recently', 'publisher', 'publication_date']
    list_filter = ['publication_date']
    search_fields = ['title']

class BookInline(admin.TabularInline):
    model = Book
    extra = 1

class PublisherAdmin(admin.ModelAdmin):
    inlines = [BookInline]
    search_fields = ['name','website', 'city', 'country']
    list_display = ['name','website', 'city', 'country']

admin.site .register(Author, AuthorAdmin)
admin.site .register(Book, BookAdmin)
admin.site .register(Publisher, PublisherAdmin)