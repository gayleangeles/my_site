
from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

from hello.views import hello


urlpatterns = patterns('',
    url(r'^hello/$', hello),
    url(r'^admin/', include(admin.site.urls)),
)