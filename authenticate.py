from django.contrib.auth import authenticate



def authenticate_function(username, password):
    user = authenticate(username=username, password=password)

    if user is not None:
        # the password verified for the user
        if user.is_active:
            print("User is valid, active and authenticated")
        else:
            print("The password is valid, but the account has been disabled!")
    else:
        # the authentication system was unable to verify the username and password
        print("The username and password were incorrect.")