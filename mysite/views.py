from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
import datetime
from django.shortcuts import render
from .forms import ContactForm, LoginForm, LogmeinForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

def logmein(request):

    if request.method == 'POST':  # If the form has been submitted...
        form = LogmeinForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    HttpResponse("Logged In")
                    login(request, user)
                    
                else:
                    HttpResponse("Account Disabled")
            else:
                HttpResponse("Invalid Login")
    else:
        form = LogmeinForm()  # An unbound form

    return render(
        request,
        'loginpage.html',
        {'form': form})


   

def contact(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = ContactForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            subject = form.cleaned_data.get('subject')
            message = form.cleaned_data.get('message')
            print subject
            print message
            return HttpResponseRedirect('/thanks/')
    else:
        form = ContactForm()  # An unbound form

    return render(
        request,
        'contact.html',
        {'form': form}
    )

def loginform(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = LoginForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            email = form.cleaned_data.get('email')
            user = User.objects.create_user(username, email, password)
            return HttpResponseRedirect('/thanks/')
    else:
        form = LoginForm()  # An unbound form

    return render(
        request,
        'loginform.html',
        {'form': form}
    )

def thanks(request):
    return HttpResponse("Tnx.")


def hello(request):
    return HttpResponse("Hello world")

def current_datetime(request):
 	now = datetime.datetime.now()
	context = ({'current_date': now})
	return render(request, 'current_datetime.html', context)

def hours_ahead(request, offset):
    offset = int(offset)
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    context2 = ({'hour_offset': offset, 'next_time':dt})
    return render(request, 'hours_ahead.html', context2)


def home(request):
	return HttpResponse("Welcome to Math!")

def add_numbers(request, num1, num2):
    try:
        num1 = int(num1)
        num2 = int(num2)
    except ValueError:
        raise Http404()
    total = num1 + num2
    html = "<html><body>The sum of {} and {} is {}.".format(num1, num2, total)
    return HttpResponse(html)

def subtract_numbers(request, num1, num2):
    try:
        num1 = int(num1)
        num2 = int(num2)
    except ValueError:
        raise Http404()
    total = num1 - num2
    html = "<html><body>The difference of {} and {} is {}.".format(num1, num2, total)
    return HttpResponse(html)


def multiply_numbers(request, num1, num2):
    try:
        num1 = int(num1)
        num2 = int(num2)
    except ValueError:
        raise Http404()
    total = num1 * num2
    html = "<html><body>The product of {} and {} is {}.".format(num1, num2, total)
    return HttpResponse(html)

def square_number(request, num):
    try:
        num = int(num)
    except ValueError:
        raise Http404()
    total = num * num
    html = "<html><body>The Square of {} is {}.".format(num, total)
    return HttpResponse(html)


# Create your views here.
