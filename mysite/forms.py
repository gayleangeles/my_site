from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(max_length=100)
	password = forms.CharField(max_length=100)
	email = forms.EmailField()

class LogmeinForm(forms.Form):
	username = forms.CharField(max_length=100)
	password = forms.CharField(max_length=100)

class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)

