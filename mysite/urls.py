from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

from.views import logmein, loginform, contact, thanks, hello, current_datetime,hours_ahead, add_numbers, home, multiply_numbers, subtract_numbers, square_number


urlpatterns = patterns('',
	url(r'^$', home),
    url(r'^hello/$', hello),
    url(r'^time/$', current_datetime),
    url(r'^time/plus(\d+)/$', hours_ahead),
    url(r'^(\d+)/$', square_number),
    url(r'^(\d+)/(\d+)/$', multiply_numbers),
    url(r'^add/(\d+)/(\d+)/$', add_numbers),
    url(r'^subtract/(\d+)/(\d+)/$', subtract_numbers),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^books/', include('books.urls')),
    url(r'^contact/$', contact),
    url(r'^register/$', loginform),
    url(r'^login/$', logmein),
    url(r'^thanks/$', thanks),
)